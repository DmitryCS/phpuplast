<h1 class="message--dark">_</h1>
<a href="#" onClick="pageTracker._trackEvent('Videos', 'Play', 'Baby\'s First Birthday');">Play</a>

<!--<h1 class="ml11">-->
<!--  <span class="text-wrapper">-->
<!--    <span class="line line1"></span>-->
<!--    <span class="letters" style="align-content: center;">Welcome</span>-->
<!--  </span>-->
<!--</h1>-->
<!---->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>-->
<!---->



<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use frontend\assets\FontAwesomeAsset;
use frontend\assets\Bootstrap4Asset;

AppAsset::register($this);
FontAwesomeAsset::register($this);
Bootstrap4Asset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-TTJB7C3');</script>
    <!-- End Google Tag Manager -->


    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TTJB7C3"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<label for="subscribe">
    <input id="subscribe" type="checkbox" name="terms-and-conditions">
    Click me
</label>

<!--<div class="message--dark">-->

<?php $this->beginBody() ?>
<div class="container container-heigh">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 image-header-block center-block" style="margin: 0px auto">
            <img src="/img/head.png" class="image-header hidden-xs">
        </div>
        <div class="col-md-2">
                                    <?= Html::beginForm(['/site/language']) ?>
                                    <?= Html::dropDownList('language', Yii::$app->language, ['en-US' => 'English', 'ru-RU' => 'Русский']) ?>
                                    <?= Html::submitButton('Change') ?>
                                    <?= Html::endForm() ?>
        </div>
    </div>
    <?php
    $menuItems = [
        ['label' => Yii::t('menu', 'Newsfeed'), 'url' => ['/site/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => Yii::t('menu', 'Signup'), 'url' => ['/user/default/signup']];
        $menuItems[] = ['label' => Yii::t('menu', 'Login'), 'url' => ['/user/default/login']];
    } else {
        $menuItems[] = ['label' => Yii::t('menu', 'My profile'), 'url' => ['/user/profile/view', 'nickname' => Yii::$app->user->identity->username]];
        $menuItems[] = ['label' => Yii::t('menu', 'Create post'), 'url' => ['/post/default/create']];
        $menuItems[] = ['label' => Yii::t('menu', 'Logout ({username})', ['username' => Yii::$app->user->identity->username]), 'url' => ['/user/default/logout']];
    }
    echo Nav::widget([
                         'options' => ['class' => 'nav nav-pills main-menu'],
                         'items' => $menuItems,
                     ]);
    ?>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 center-block ">
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
        <div class="col-md-2"></div>

        <div class="push"></div>
    </div>
</div>

<footer class="footer-heigh">
    <div class="footer">
<!--        <div class="back-to-top-page">-->
<!--            <a class="back-to-top"><i class="fa fa-angle-double-up"></i></a>-->
<!--        </div>-->
        <p class="text"><?php echo Yii::t('about', 'Images project | {currentYear}', ['currentYear' => date('Y')]); ?></p>
    </div>
</footer>

<?php $this->endBody() ?>

<!--</div>-->
</body>
</html>
<?php $this->endPage() ?>
<!-- Button trigger modal -->

<!--<script>-->
<!--    // Wrap every letter in a span-->
<!--    var textWrapper = document.querySelector('.ml11 .letters');-->
<!--    textWrapper.innerHTML = textWrapper.textContent.replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>");-->
<!---->
<!--    anime.timeline({loop: true})-->
<!--    .add({-->
<!--    targets: '.ml11 .line',-->
<!--    scaleY: [0,1],-->
<!--    opacity: [0.5,1],-->
<!--    easing: "easeOutExpo",-->
<!--    duration: 700-->
<!--    })-->
<!--    .add({-->
<!--    targets: '.ml11 .line',-->
<!--    translateX: [0, document.querySelector('.ml11 .letters').getBoundingClientRect().width + 10],-->
<!--    easing: "easeOutExpo",-->
<!--    duration: 700,-->
<!--    delay: 100-->
<!--    }).add({-->
<!--    targets: '.ml11 .letter',-->
<!--    opacity: [0,1],-->
<!--    easing: "easeOutExpo",-->
<!--    duration: 600,-->
<!--    offset: '-=775',-->
<!--    delay: (el, i) => 34 * (i+1)-->
<!--    }).add({-->
<!--    targets: '.ml11',-->
<!--    opacity: 0,-->
<!--    duration: 1000,-->
<!--    easing: "easeOutExpo",-->
<!--    delay: 3000-->
<!--    });-->
<!--</script>-->