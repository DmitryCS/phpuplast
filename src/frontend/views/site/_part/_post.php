<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;


$post = \common\models\Post::findOne(['id' => $feedItem->post_id]);
?>
<div class="row center-block  block-post">
    <div class="col-md-12 center-block">
        <div class="thumbnail">
            <div class="media" style="; padding-bottom: 10px;">
                <div style="display: inline-block">
                    <img src="<?= $feedItem->author_picture; ?>" class="mr-3 img-circle" alt="user"
                         style="width: 50px;">

                    <span class="mt-0 mb-1">
                      <a href="<?= Url::to(['/user/profile/view', 'nickname' => ($feedItem->author_nickname) ? $feedItem->author_nickname : $feedItem->author_id]); ?>">
                        <?php echo Html::encode($feedItem->author_name); ?>
                          </a>
                    </span>
                </div>
            </div>

            <img src="<?php echo Yii::$app->storage->getFile($feedItem->post_filename); ?>"
                 class="img-thumbnailcard-img-top" alt="cat" style="width: 100%;">
            <div class="caption">
                <ul class="list-unstyled" style="padding-bottom: 0px;margin-bottom: 5px;">
                    <li class="media">


                        <div class="col-md-12" style="text-align: center;">

                            <a href="#"
                               class="button-unlike <?php echo ($currentUser && $post->isLikedBy($currentUser)) ? "" : "display-none"; ?>"
                               data-id="<?php echo $post->id; ?>">
                                &nbsp;&nbsp;<span class="glyphicon glyphicon-heart"></span>
                            </a>
                            <a href="#"
                               class="button-like <?php echo ($currentUser && $post->isLikedBy($currentUser)) ? "display-none" : ""; ?>"
                               data-id="<?php echo $post->id; ?>">
                                &nbsp;&nbsp;<span class="glyphicon glyphicon-heart-empty"></span>
                            </a>

                            <span class="likes-count">Лайков: <?php echo $post->countLikes(); ?></span>
                            <?php if (!$feedItem->isReported($currentUser)): ?>
                                <a href="#" class="btn btn-default button-complain"
                                   data-id="<?php echo $feedItem->post_id; ?>">
                                    Report post <i class="fa fa-cog fa-spin fa-fw icon-preloader"
                                                   style="display:none"></i>
                                </a>
                            <?php else: ?>
                                <p>Post has been reported</p>
                            <?php endif; ?>
                        </div>
                        <div class="post-report">
                        </div>
                        <div class="media-body" style="padding-left: 15px; padding-top: 7px;">
                            <p class="card-text"><?php echo HtmlPurifier::process($feedItem->post_description); ?>
                                <a href="<?= Url::to(['/post/default/view', 'id' => $feedItem->post_id]) ?>">
                                    (Подробнее) </a>
                            </p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</div>
