<?php



/**
 * @var User $user
 * @var User $currentUser
 * @var \frontend\modules\user\models\forms\PictureForm $modelPicture
 */

use common\models\User;
use dosamigos\fileupload\FileUpload;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

?>
<p><?php echo HtmlPurifier::process($user->about); ?></p>
<hr>

<div style="display: inline-block; vertical-align: center;">
    <img src="<?php echo $user->getPicture(); ?>" id="profile-picture" class="mr-3 rounded-circle avatar-user-100"
         alt="user">
    <span style="font-weight: bold;"><?= $user->username ?></span>
</div>

<?php if ($currentUser): ?>
    <?php if ($currentUser->equals($user)): ?>
        <div class="alert alert-success display-none" id="profile-image-success">Обновить изображение</div>
        <div class="alert alert-danger display-none" id="profile-image-fail"></div>

        <?= FileUpload::widget([
            'model' => $modelPicture,
            'attribute' => 'picture',
            'url' => ['/user/profile/upload-picture'],
            'options' => ['accept' => 'image/*'],
            'clientEvents' => [
                'fileuploaddone' => 'function(e, data) {
                if (data.result.success) {
                    $("#profile-image-success").show();
                    $("#profile-image-fail").hide();
                    $("#profile-picture").attr("src", data.result.pictureUri);
                } else {
                    $("#profile-image-fail").html(data.result.errors.picture).show();
                    $("#profile-image-success").hide();
                }
            }',
            ],
        ]); ?>

    <?php endif; ?>
    <?php if (!$currentUser->equals($user)): ?>
        &nbsp;
        <a href="<?php echo Url::to(['/user/profile/subscribe', 'id' => $user->getId()]); ?>" class="btn btn-default">Подписаться</a>
        &nbsp
        <a href="<?php echo Url::to(['/user/profile/unsubscribe', 'id' => $user->getId()]); ?>" class="btn btn-default">Отписаться</a>

        <h5> Участники, на которых вы можете подписаться, <?php echo Html::encode($user->username); ?>: </h5>
        <div class="row">
            <?php foreach ($currentUser->getMutualSubscriptionsTo($user) as $item): ?>
                <div class="col-md-12">
                    <a href="<?php echo Url::to([
                        '/user/profile/view',
                        'nickname' => ($item['nickname']) ? $item['nickname'] : $item['id'],
                    ]); ?>">
                        <?php echo Html::encode($item['username']); ?>
                    </a>
                </div>
            <?php endforeach; ?>

        </div>
    <?php endif; ?>
    <hr>
<?php endif; ?>

<!-- Button trigger modal -->
<button type="button" class="btn btn-default btn-md" data-toggle="modal" data-target="#myModal1">
    Подписки: <?php echo $user->countSubscriptions(); ?>
</button>

<!-- Button trigger modal -->
<button type="button" class="btn btn-default btn-md" data-toggle="modal" data-target="#myModal2">
    Подписчики: <?php echo $user->countFollowers(); ?>
</button>


<!-- Modal subscriptions -->
<div class="modal fade" id="myModal1" tabindex="1111" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Subscriptions</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <?php foreach ($user->getSubscriptions() as $subscription): ?>
                        <div class="col-md-12">
                            <a href="<?php echo Url::to([
                                '/user/profile/view',
                                'nickname' => ($subscription['nickname']) ? $subscription['nickname'] : $subscription['id'],
                            ]); ?>">
                                <?php echo Html::encode($subscription['username']); ?>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal subscriptions -->

<!-- Modal followers -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Followers</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <?php foreach ($user->getFollowers() as $follower): ?>
                        <div class="col-md-12">
                            <a href="<?php echo Url::to([
                                '/user/profile/view',
                                'nickname' => ($follower['nickname']) ? $follower['nickname'] : $follower['id'],
                            ]); ?>">
                                <?php echo Html::encode($follower['username']); ?>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal followers -->

<div class="mx-auto">
    <?php

    $posts = \common\models\Post::findAll(['user_id' => $user->id]);

    foreach ($posts as $post) : ?>
        <img src="<?= $post->getImage() ?>" class="img-thumbnail float-left card-img-top img-mini "
             alt="cat">

    <?php
    endforeach;
    ?>


</div>










