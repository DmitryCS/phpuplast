<?php

namespace frontend\components;

use common\models\Post;
use frontend\modules\post\models\events\PostCreatedEvent;
use yii\base\Component;

use common\models\Feed;
use common\models\User;

/**
 * Feed component
 *
 * @author admin
 */
class FeedService extends Component
{

    /**
     * Add post to feed of author subscribers
     * @param PostCreatedEvent $event
     */
    public function addToFeeds(\yii\base\Event $event)
    {




        /* @var $user User */
        $user = $event->getUser();
        /* @var $post Post */
        $post = $event->getPost();

        $followers = $user->getFollowers();
//        echo "<pre>";
//        var_export($followers);
//        die("dd");
        foreach ($followers as $follower) {
            $feedItem = new Feed();
            $feedItem->user_id = $follower['id'];
            $feedItem->author_id = $user->id;
            $feedItem->author_name = $user->username;
            $feedItem->author_nickname = $user->getNickname();
            $feedItem->author_picture = $user->getPicture();
            $feedItem->post_id = $post->id;
            $feedItem->post_filename = $post->filename;
            $feedItem->post_description = $post->description;
            $feedItem->post_created_at = $post->created_at;
            $feedItem->save();
        }
    }

}
